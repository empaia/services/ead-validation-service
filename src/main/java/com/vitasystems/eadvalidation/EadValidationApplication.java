package com.vitasystems.eadvalidation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EadValidationApplication {
    public static void main( String[] args ) {
        SpringApplication.run( EadValidationApplication.class, args );
    }
}
