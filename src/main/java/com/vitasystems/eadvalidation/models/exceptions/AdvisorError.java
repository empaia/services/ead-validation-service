package com.vitasystems.eadvalidation.models.exceptions;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Builder
@Data
public class AdvisorError {
    private String error;

    @JsonProperty( "error_description" )
    private String errorDescription;

    private Map<String, String> errors;

    private Object timestamp;
}

