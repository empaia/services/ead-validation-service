package com.vitasystems.eadvalidation.models.exceptions;

import lombok.Getter;

import java.util.Map;

@Getter
public class EADValidationException extends RuntimeException {
    private String title;

    private Map<String, String> errors;

    public EADValidationException( String title, String message, Map<String, String> errors ) {
        super( message );

        this.title = title;
        this.errors = errors;
    }
}
