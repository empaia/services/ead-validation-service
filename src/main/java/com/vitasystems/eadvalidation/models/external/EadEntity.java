package com.vitasystems.eadvalidation.models.external;

import java.util.Map;

public interface EadEntity {
    String getChecksum();

    Map<String, Object> getContent();
}
