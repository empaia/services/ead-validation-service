package com.vitasystems.eadvalidation.models.constants;

public class Keywords {
    public static final String CLASSES_NAMESPACE_START   = "org.empaia";
    public static final String GLOBAL_NAMESPACE_PREFIX   = "org.empaia.global";
    public static final String NAMESPACE_REGEX_SEPARATOR = "\\.";
    public static final String NAMESPACE_SEPARATOR       = ".";

    public static final String INPUTS      = "inputs";
    public static final String OUTPUTS     = "outputs";
    public static final String ITEMS       = "items";
    public static final String REFERENCE   = "reference";
    public static final String CLASSES     = "classes";
    public static final String DESCRIPTION = "description";
    public static final String NAME        = "name";
    public static final String MAPPING     = "mapping";
    public static final String NAMESPACE   = "namespace";
    public static final String TYPE        = "type";
}
