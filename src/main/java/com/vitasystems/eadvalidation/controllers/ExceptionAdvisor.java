package com.vitasystems.eadvalidation.controllers;

import com.vitasystems.eadvalidation.models.exceptions.AdvisorError;
import com.vitasystems.eadvalidation.models.exceptions.EADValidationException;
import lombok.extern.slf4j.Slf4j;
import org.everit.json.schema.ValidationException;
import org.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class ExceptionAdvisor extends ResponseEntityExceptionHandler {
    @ExceptionHandler( EADValidationException.class )
    public ResponseEntity<Object> handleEadValidationException( WebRequest request, EADValidationException e ) {
        return handler(
            request,
            e,
            e.getTitle(),
            e.getMessage(),
            e.getErrors(),
            HttpStatus.BAD_REQUEST,
            true
        );
    }

    @ExceptionHandler( ValidationException.class )
    public ResponseEntity<Object> handleValidationException( WebRequest request, ValidationException e ) {
        Map<String, String> errors;

        if ( e.getCausingExceptions().isEmpty() ) {
            errors = new HashMap<>();
            errors.put( "1", e.getErrorMessage() );
        } else {
            List<String> entries = new ArrayList<>();

            errors = e.getCausingExceptions()
                      .stream()
                      .map( f -> {
                          entries.add( f.getSchemaLocation() );

                          return Map.entry( String.format( "%d.%s", entries.size(), f.getViolationCount() ), f.getMessage() );
                      } )
                      .collect( Collectors.toMap( Map.Entry::getKey, Map.Entry::getValue ) );
        }

        return handler(
            request,
            e,
            "Invalid JSON input",
            "Unable to validate json",
            errors,
            HttpStatus.BAD_REQUEST,
            true
        );
    }

    @ExceptionHandler( JSONException.class )
    public ResponseEntity<Object> handleValidationException( WebRequest request, JSONException e ) {
        return handler(
            request,
            e,
            "Invalid JSON input",
            e.getMessage(),
            HttpStatus.BAD_REQUEST,
            false
        );
    }

    private ResponseEntity<Object> handler(
        WebRequest request, Exception e, String title, String description, HttpStatus status, boolean doLog
    ) {
        AdvisorError error = AdvisorError.builder()
                                         .error( title )
                                         .errorDescription( description )
                                         .timestamp( LocalDateTime.now() )
                                         .build();

        if ( doLog ) {
            log.error( "Error on request: {}", request.getDescription( true ) );
            log.error( title, e );
        }

        return new ResponseEntity<>( error, status );
    }

    private ResponseEntity<Object> handler(
        WebRequest request,
        Exception e,
        String title,
        String description,
        Map<String, String> errorMessages,
        HttpStatus status,
        boolean doLog
    ) {
        AdvisorError error = AdvisorError.builder()
                                         .error( title )
                                         .errorDescription( description )
                                         .errors( errorMessages )
                                         .timestamp( LocalDateTime.now() )
                                         .build();

        if ( doLog ) {
            log.error( "Error on request: {}", request.getDescription( true ) );
            log.error( title, e );
        }

        return new ResponseEntity<>( error, status );
    }

    private String getFieldErrorMessage( FieldError error, String defaultMessage ) {
        return error.getDefaultMessage() == null ? defaultMessage : error.getDefaultMessage();
    }
}
