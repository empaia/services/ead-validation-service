package com.vitasystems.eadvalidation.controllers;

import com.vitasystems.eadvalidation.services.validation.EadValidationService;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class ValidationController {
    private final EadValidationService validationService;

    public ValidationController( EadValidationService validationService ) {
        this.validationService = validationService;
    }

    @RequestMapping("/api/v0/validate")
    public String validate( @RequestBody String ead ) {
        JSONObject validatedEad = validationService.validateAndParse( ead );

        return validationService.computeChecksum( validatedEad );
    }

    @RequestMapping("/alive")
    public String alive( ) {
        return "{\"status\":\"ok\"}";
    }
}

