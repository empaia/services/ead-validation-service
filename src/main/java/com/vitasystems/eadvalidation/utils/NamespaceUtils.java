package com.vitasystems.eadvalidation.utils;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.vitasystems.eadvalidation.models.constants.Keywords.*;

public class NamespaceUtils {
    public static Optional<String> extractNamespaceFromClassName( String className ) {
        String[] parts    = className.split( NAMESPACE_REGEX_SEPARATOR );
        boolean  isGlobal = className.startsWith( GLOBAL_NAMESPACE_PREFIX );

        int minimumLength = isGlobal ? 5 : 6;

        if ( parts.length < minimumLength ) {
            return Optional.empty();
        }

        String[] namespace = Arrays.copyOfRange( parts, 0, minimumLength - 1 );

        return Optional.of( String.join( NAMESPACE_SEPARATOR, namespace ) );
    }

    public static Optional<String> extractPathFromClassNamespace( String className ) {
        String[] parts    = className.split( NAMESPACE_REGEX_SEPARATOR );
        boolean  isGlobal = className.startsWith( GLOBAL_NAMESPACE_PREFIX );

        int minimumLength = isGlobal ? 5 : 6;

        if ( parts.length < minimumLength ) {
            return Optional.empty();
        }

        String[] namespace = Arrays.copyOfRange( parts, minimumLength - 1, parts.length );

        return Optional.of( String.join( NAMESPACE_SEPARATOR, namespace ) );
    }

    public static Map<String, String> isValidPath( String jsonPath, JSONObject object ) {
        Map<String, String> errors     = new HashMap<>();
        String[]            path       = jsonPath.split( NAMESPACE_REGEX_SEPARATOR );
        JSONObject          pathFinder = object;

        for ( String s : path ) {
            if ( pathFinder.has( s ) ) {
                pathFinder = pathFinder.getJSONObject( s );
            } else {
                errors.put( jsonPath, "Unable to resolve ref" );
            }
        }

        return errors;
    }
}
