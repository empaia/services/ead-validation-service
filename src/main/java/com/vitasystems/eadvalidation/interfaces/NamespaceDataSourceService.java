package com.vitasystems.eadvalidation.interfaces;

import com.vitasystems.eadvalidation.models.external.EadEntity;

import java.util.List;

public interface NamespaceDataSourceService {
    boolean isNamespacePresent( String namespace );

    List<EadEntity> findAllByNamespace( String namespace );
}
