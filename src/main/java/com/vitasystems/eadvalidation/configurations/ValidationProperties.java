package com.vitasystems.eadvalidation.configurations;

import com.vitasystems.eadvalidation.interfaces.NamespaceDataSourceService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("ead-validation")
@Getter
@Setter
public class ValidationProperties {
    /**
     * Enable to use a data source to validate namespace references to external ead
     * You will need to declare a NamespaceDataSourceService
     */
    private boolean validateNamespaceReferences;

    /**
     * Data source, required if namespace validation is enabled
     */
    private NamespaceDataSourceService namespaceDataSourceService;

    /**
     * Optional schema location
     */
    private String schemaLocation;
}
