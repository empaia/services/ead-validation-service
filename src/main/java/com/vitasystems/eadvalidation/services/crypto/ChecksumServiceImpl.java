package com.vitasystems.eadvalidation.services.crypto;

import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Optional;
import java.util.zip.CRC32;

@Service
public class ChecksumServiceImpl implements ChecksumService {
    @Override
    public Optional<String> safeChecksum( Object object ) {
        CRC32       crc32 = new CRC32();
        InputStream bytes = null;

        try {
            bytes = objectToStream( object );
        } catch ( IOException e ) {
            return Optional.empty();
        }

        try {
            crc32.update( bytes.readAllBytes(), 0, bytes.readAllBytes().length );

            return Optional.of( String.valueOf( crc32.getValue() ) );
        } catch ( IOException e ) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<String> safeChecksum( String object ) {
        CRC32 crc32 = new CRC32();
        crc32.update( object.getBytes(), 0, object.getBytes().length );

        return Optional.of( String.valueOf( crc32.getValue() ) );
    }

    private InputStream objectToStream( Object obj ) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream    oos  = new ObjectOutputStream( baos );

        oos.writeObject( obj );
        oos.flush();
        oos.close();

        return new ByteArrayInputStream( baos.toByteArray() );
    }
}
