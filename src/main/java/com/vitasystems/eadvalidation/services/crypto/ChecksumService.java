package com.vitasystems.eadvalidation.services.crypto;

import java.util.Optional;

public interface ChecksumService {
    /**
     * Takes an object and checksums it in a reproducible way.
     *
     * @param object An object
     * @return The checksum
     */
    Optional<String> safeChecksum( Object object );

    /**
     * Takes a string and checksums it in a reproducible way.
     *
     * @param object A string
     * @return The checksum
     */
    Optional<String> safeChecksum( String object );
}
