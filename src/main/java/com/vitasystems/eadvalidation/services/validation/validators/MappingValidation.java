package com.vitasystems.eadvalidation.services.validation.validators;

import com.vitasystems.eadvalidation.interfaces.NamespaceDataSourceService;
import com.vitasystems.eadvalidation.models.exceptions.EADValidationException;
import com.vitasystems.eadvalidation.models.external.EadEntity;
import com.vitasystems.eadvalidation.services.validation.EadValidator;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.vitasystems.eadvalidation.models.constants.Keywords.*;
import static com.vitasystems.eadvalidation.utils.NamespaceUtils.*;

public class MappingValidation implements EadValidator {
    private final boolean                    validateNamespaces;
    private final NamespaceDataSourceService namespaceService;

    public MappingValidation( boolean validateNamespaces, NamespaceDataSourceService namespaceService ) {
        this.validateNamespaces = validateNamespaces;
        this.namespaceService = namespaceService;
    }

    /**
     * Fifth step of the validation pipeline
     * Verifies the mappings validity
     *
     * @param ead JSON ead
     * @return EAD if its valid
     */
    @Override
    public JSONObject validateOrThrow( JSONObject ead ) {
        Map<String, String> errors = new HashMap<>();

        if ( ead.has( CLASSES ) ) {
            JSONObject classes = ead.getJSONObject( CLASSES );

            // Iterate all inputs and check references
            classes.keys()
                   .forEachRemaining( k -> errors.putAll( recursiveMappingCheck( classes.getJSONObject( k ), ead ) ) );
        }

        // If we have an error we need to throw
        if ( !errors.isEmpty() ) {
            throw new EADValidationException(
                "Invalid mappings",
                "One or multiple mappings were not valid",
                errors
            );
        }

        return ead;
    }

    private Map<String, String> recursiveMappingCheck( JSONObject current, JSONObject ead ) {
        Map<String, String> errors        = new HashMap<>();
        List<String>        validKeywords = List.of( NAME, DESCRIPTION, MAPPING );

        if ( current.has( MAPPING ) ) {
            String classRef = current.getString( MAPPING );

            String eadNamespace   = ead.getString( NAMESPACE );
            String classNamespace = extractNamespaceFromClassName( classRef ).orElse( "" );
            String classPath      = extractPathFromClassNamespace( classRef ).orElse( classNamespace );

            if ( eadNamespace.equals( classNamespace ) ) {
                errors.putAll( isValidPath( classRef, ead ) );
            } else if( validateNamespaces ) { // We only want to trigger this code if namespace validation is enabled
                List<EadEntity> appsMatched = namespaceService.findAllByNamespace( classNamespace );

                // If no apps matched we know the namespace is not valid
                if ( appsMatched.isEmpty() ) {
                    errors.put( classRef, "Namespace not found" );
                } else {
                    errors.putAll( isValidPath( classPath, new JSONObject( appsMatched.get( 0 ).getContent() ) ) );
                }
            }
        }

        // If we have other mappings then we should check them as well
        current.keys().forEachRemaining( k -> {
            if ( !validKeywords.contains( k ) ) {
                errors.putAll( recursiveMappingCheck( current.getJSONObject( k ), ead ) );
            }
        } );

        return errors;
    }
}
