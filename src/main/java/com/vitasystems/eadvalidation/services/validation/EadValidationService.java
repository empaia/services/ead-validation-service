package com.vitasystems.eadvalidation.services.validation;

import com.vitasystems.eadvalidation.models.exceptions.EADValidationException;
import org.everit.json.schema.ValidationException;
import org.json.JSONObject;

public interface EadValidationService {
    /**
     * Computes a checksum for the classes of the EAD
     *
     * @param ead The JSON object representing the EAD
     * @return A checksum
     */
    String computeChecksum( JSONObject ead );

    JSONObject validateAndParse( String ead ) throws EADValidationException, ValidationException;
}
