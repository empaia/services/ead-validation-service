package com.vitasystems.eadvalidation.services.validation.validators;

import com.vitasystems.eadvalidation.interfaces.NamespaceDataSourceService;
import com.vitasystems.eadvalidation.models.exceptions.EADValidationException;
import com.vitasystems.eadvalidation.models.external.EadEntity;
import com.vitasystems.eadvalidation.services.crypto.ChecksumService;
import com.vitasystems.eadvalidation.services.validation.EadValidator;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.vitasystems.eadvalidation.models.constants.Keywords.NAMESPACE;

public class NamespaceValidation implements EadValidator {
    private final boolean                    validateNamespaces;
    private final NamespaceDataSourceService namespaceService;
    private final ChecksumService            checksumService;

    public NamespaceValidation(
        boolean validateNamespaces,
        NamespaceDataSourceService namespaceService,
        ChecksumService checksumService
    ) {
        this.validateNamespaces = validateNamespaces;
        this.namespaceService = namespaceService;
        this.checksumService = checksumService;
    }

    /**
     * Second step of the validation pipeline
     * Verifies the classes and computes checksum for it
     * It requires the namespace service to be provided and validateNamespace to be enabled
     *
     * @param ead JSON ead
     * @return EAD if its valid
     */
    @Override
    public JSONObject validateOrThrow( JSONObject ead ) {
        // Return if namespace validation isn't toggled
        if ( !validateNamespaces ) return ead;

        String namespace = ead.getString( NAMESPACE );
        String checksum  = checksumService.safeChecksum( ead ).orElse( "" );

        List<EadEntity> namespaceMatches = namespaceService.findAllByNamespace( namespace );

        // If namespace doesn't exists yet we can use it
        if ( namespaceMatches.isEmpty() ) {
            return ead;
        }

        // Check if the namespace hashes are the same, otherwise namespace is not valid
        for ( EadEntity namespaceMatch : namespaceMatches ) {
            if ( namespaceMatch.getChecksum() == null ) {
                continue;
            }

            if ( !namespaceMatch.getChecksum().equals( checksum ) ) {
                // If they do not match we need to throw a validation exception
                Map<String, String> errors = new HashMap<>();

                errors.put( "classes_do_not_match", "Error validating hash" );

                throw new EADValidationException(
                    "Namespace collision",
                    "The given namespace already exists and does not match",
                    errors
                );
            }
        }

        return ead;
    }
}
