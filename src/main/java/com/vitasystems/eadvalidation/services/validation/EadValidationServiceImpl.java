package com.vitasystems.eadvalidation.services.validation;

import com.vitasystems.eadvalidation.configurations.ValidationProperties;
import com.vitasystems.eadvalidation.interfaces.NamespaceDataSourceService;
import com.vitasystems.eadvalidation.models.exceptions.EADValidationException;
import com.vitasystems.eadvalidation.services.crypto.ChecksumService;
import com.vitasystems.eadvalidation.services.validation.validators.ClassValidation;
import com.vitasystems.eadvalidation.services.validation.validators.MappingValidation;
import com.vitasystems.eadvalidation.services.validation.validators.NamespaceValidation;
import com.vitasystems.eadvalidation.services.validation.validators.ReferenceValidation;
import lombok.extern.slf4j.Slf4j;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static com.vitasystems.eadvalidation.models.constants.Keywords.*;

@Service
@EnableConfigurationProperties( ValidationProperties.class )
@Slf4j
public class EadValidationServiceImpl implements EadValidationService {
    private static final String CURRENT_EAD_SCHEMA_PATH = "/schemas/current-version.json";

    private final boolean                    validateNamespace;
    private final NamespaceDataSourceService namespaceService;
    private final ChecksumService            checksumService;
    private final JSONObject                 schema;

    public EadValidationServiceImpl(
        ValidationProperties validationProperties,
        ChecksumService checksumService
    ) throws IOException {
        this.validateNamespace = validationProperties.isValidateNamespaceReferences();
        this.namespaceService = validationProperties.getNamespaceDataSourceService();
        this.checksumService = checksumService;

        if ( validationProperties.isValidateNamespaceReferences() && validationProperties.getNamespaceDataSourceService() == null ) {
            log.error( "If namespace validation is enabled the NamespaceDataSourceService is required" );
            throw new NullPointerException( "NamespaceDataSourceService was null" );
        }

        // The JSON schema to validate against
        // We load it and keep it cached
        // Loading it here allows us to prevent the app from running if the schema is not present
        InputStream schemaStream;

        if( validationProperties.getSchemaLocation() != null && !validationProperties.getSchemaLocation().isEmpty() ) {
            log.info( "Loading schema from filesystem '{}'", validationProperties.getSchemaLocation() );
            try {
                schemaStream = Files.newInputStream( Path.of( validationProperties.getSchemaLocation() ) );
            } catch ( Exception e ) {
                log.error( String.format( "Unable to load json schema from '%s'", validationProperties.getSchemaLocation() ) );
                schemaStream = null;
            }
        } else {
            log.info( "Loading baked-in schema" );
            schemaStream = getClass().getResourceAsStream( CURRENT_EAD_SCHEMA_PATH );
        }

        if ( schemaStream == null ) {
            throw new IOException( "Unable to stream file, aborting..." );
        }

        schema = new JSONObject( new JSONTokener( schemaStream ) );
    }

    @Override
    public String computeChecksum( JSONObject ead ) {
        if ( ead.has( CLASSES ) ) {
            return checksumService.safeChecksum( ead.getJSONObject( CLASSES ).toString() ).orElse( "" );
        }

        return "";
    }

    @Override
    public JSONObject validateAndParse( String ead ) throws EADValidationException, ValidationException {
        // Validate schema
        JSONObject eadJson = validateSchemaOrThrow( ead );

        // Init pipeline
        List<EadValidator> validators = createValidationPipeline();

        // Run every step
        for ( EadValidator validator : validators ) {
            eadJson = validator.validateOrThrow( eadJson );
        }

        return eadJson;
    }

    /**
     * First step of the validation pipeline
     * Verifies the json integrity
     *
     * @param ead String version of the ead
     * @return The ead as JSON
     */
    private JSONObject validateSchemaOrThrow( String ead ) throws ValidationException {
        JSONObject eadJson = new JSONObject( ead );

        // Enable V7 Json
        SchemaLoader loader = SchemaLoader.builder()
                                          .schemaJson( schema )
                                          .draftV7Support()
                                          .build();

        // Will throw an error if the schema is not valid
        loader.load().build().validate( eadJson );

        return eadJson;
    }

    /**
     * Initializes all validation steps in correct order
     *
     * @return A list of validation steps
     */
    private List<EadValidator> createValidationPipeline() {
        return List.of(
            new NamespaceValidation( validateNamespace, namespaceService, checksumService ),
            new ReferenceValidation(),
            new ClassValidation( validateNamespace, namespaceService ),
            new MappingValidation( validateNamespace, namespaceService )
        );
    }
}
