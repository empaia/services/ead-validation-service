package com.vitasystems.eadvalidation.services.validation;

import org.json.JSONObject;

public interface EadValidator {
    public JSONObject validateOrThrow( JSONObject ead );
}
