package com.vitasystems.eadvalidation.services.validation.validators;

import com.vitasystems.eadvalidation.interfaces.NamespaceDataSourceService;
import com.vitasystems.eadvalidation.models.exceptions.EADValidationException;
import com.vitasystems.eadvalidation.models.external.EadEntity;
import com.vitasystems.eadvalidation.services.validation.EadValidator;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.vitasystems.eadvalidation.models.constants.Keywords.*;
import static com.vitasystems.eadvalidation.utils.NamespaceUtils.*;

public class ClassValidation implements EadValidator {
    private final boolean                    validateNamespaces;
    private final NamespaceDataSourceService namespaceService;

    public ClassValidation( boolean validateNamespaces, NamespaceDataSourceService namespaceService ) {
        this.validateNamespaces = validateNamespaces;
        this.namespaceService = namespaceService;
    }

    /**
     * Fourth step of the validation pipeline
     * Verifies the classes
     *
     * @param ead JSON ead
     * @return EAD if its valid
     */
    @Override
    public JSONObject validateOrThrow( JSONObject ead ) {
        Map<String, String> errors = new HashMap<>();

        // If ead has inputs
        if ( ead.has( INPUTS ) ) {
            JSONObject inputs = ead.getJSONObject( INPUTS );

            // Iterate all inputs and check references
            inputs.keys()
                  .forEachRemaining( k -> errors.putAll( recursiveClassesCheck( inputs.getJSONObject( k ), ead ) ) );
        }

        // If ead has outputs
        if ( ead.has( OUTPUTS ) ) {
            JSONObject outputs = ead.getJSONObject( OUTPUTS );

            // Iterate all outputs and check references
            outputs.keys()
                   .forEachRemaining( k -> errors.putAll( recursiveClassesCheck( outputs.getJSONObject( k ), ead ) ) );
        }

        // If we have an error we need to throw
        if ( !errors.isEmpty() ) {
            throw new EADValidationException(
                "Invalid classes",
                "One or multiple classes were not valid",
                errors
            );
        }

        return ead;
    }

    private Map<String, String> recursiveClassesCheck( JSONObject current, JSONObject ead ) {
        Map<String, String> errors = new HashMap<>();

        // If has items we need to recurse
        if ( current.has( ITEMS ) ) {
            errors.putAll( recursiveClassesCheck( current.getJSONObject( ITEMS ), ead ) );
        }

        // If we do not have classes we can return errors
        if ( !current.has( CLASSES ) ) {
            return errors;
        }

        // If we have classes we must have a reference key
        if ( !current.has( REFERENCE ) ) {
            errors.put(
                "Classes definition error",
                "If a class is defined then the object must reference either an input or output"
            );
        }

        // Else we can start working on the classes checking
        JSONArray array = current.getJSONArray( CLASSES );

        for ( Object classRefObject : array ) {
            // The Schema ensures that the classRef are strings
            String classRef = (String) classRefObject;

            // If the namespace doesn't starts with org.empaia its incorrect
            if ( !classRef.startsWith( CLASSES_NAMESPACE_START ) ) {
                // If the reference doesn't point to i/o then add an error
                errors.put( classRef, "A class reference should point to org.empaia.(...)" );
                continue;
            }

            // Extract namespace and verify if its ead namespace, or if its in database
            String eadNamespace   = ead.getString( NAMESPACE );
            String classNamespace = extractNamespaceFromClassName( classRef ).orElse( "" );
            String classPath      = extractPathFromClassNamespace( classRef ).orElse( classNamespace );

            // Verify that the reference points to something valid
            if ( eadNamespace.equals( classNamespace ) ) {
                errors.putAll( isValidPath( classPath, ead ) );
                continue;
            }

            // If external namespace validation is not enabled we should skip this step
            if( validateNamespaces ) {
                List<EadEntity> appsMatched = namespaceService.findAllByNamespace( classNamespace );

                // If no apps matched we know the namespace is not valid
                if ( appsMatched.isEmpty() ) {
                    errors.put( classRef, "Namespace not found" );
                    continue;
                }

                // Since all apps with same namespace should have the same classes
                // we can just take any match and validate against
                errors.putAll( isValidPath( classPath, new JSONObject( appsMatched.get( 0 ).getContent() ) ) );
            }
        }

        return errors;
    }
}
