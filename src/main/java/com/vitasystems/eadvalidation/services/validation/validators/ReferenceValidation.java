package com.vitasystems.eadvalidation.services.validation.validators;

import com.vitasystems.eadvalidation.models.exceptions.EADValidationException;
import com.vitasystems.eadvalidation.services.validation.EadValidator;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.vitasystems.eadvalidation.models.constants.Keywords.*;
import static com.vitasystems.eadvalidation.utils.NamespaceUtils.isValidPath;

public class ReferenceValidation implements EadValidator {
    private static final List<String> annotationTypes = List.of( "point", "line", "arrow", "rectangle", "polygon", "circle" );

    /**
     * Third step of the validation pipeline
     * Verifies the references and what they are pointing to
     *
     * @param ead JSON ead
     * @return EAD if its valid
     */
    @Override
    public JSONObject validateOrThrow( JSONObject ead ) {
        Map<String, String> errors = new HashMap<>();

        // If ead has inputs
        if ( ead.has( INPUTS ) ) {
            JSONObject inputs = ead.getJSONObject( INPUTS );

            // Iterate all inputs and check references
            inputs.keys()
                  .forEachRemaining( k -> errors.putAll( recursiveReferenceCheck( inputs.getJSONObject( k ), ead ) ) );
        }

        // If ead has outputs
        if ( ead.has( OUTPUTS ) ) {
            JSONObject outputs = ead.getJSONObject( OUTPUTS );

            // Iterate all outputs and check references
            outputs.keys()
                   .forEachRemaining( k -> errors.putAll( recursiveReferenceCheck(
                       outputs.getJSONObject( k ),
                       ead
                   ) ) );
        }

        // If we have an error we need to throw
        if ( !errors.isEmpty() ) {
            throw new EADValidationException(
                "Invalid Reference",
                "One or multiple references were not valid",
                errors
            );
        }

        return ead;
    }

    private Map<String, String> recursiveReferenceCheck( JSONObject current, JSONObject ead ) {
        Map<String, String> errors = new HashMap<>();

        // If the object contains a reference we need to check it
        if ( current.has( REFERENCE ) ) {
            String reference = current.getString( REFERENCE );

            if ( !reference.startsWith( INPUTS ) && !reference.startsWith( OUTPUTS ) ) {
                // If the reference doesn't point to i/o then add an error
                errors.put( reference, "A reference must point to inputs or outputs" );
            } else {
                // Verify that the reference points to something valid
                errors.putAll( isValidPath( reference, ead ) );
            }
        }

        // If we have items, we need to also run the check on them
        if ( current.has( ITEMS ) ) {
            errors.putAll( recursiveReferenceCheck( current.getJSONObject( ITEMS ), ead ) );
        }

        return errors;
    }
}
