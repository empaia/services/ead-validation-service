package com.vitasystems.eadvalidation;

import com.vitasystems.eadvalidation.configurations.ValidationProperties;
import com.vitasystems.eadvalidation.models.exceptions.EADValidationException;
import com.vitasystems.eadvalidation.services.crypto.ChecksumService;
import com.vitasystems.eadvalidation.services.crypto.ChecksumServiceImpl;
import com.vitasystems.eadvalidation.services.validation.EadValidationService;
import com.vitasystems.eadvalidation.services.validation.EadValidationServiceImpl;
import org.everit.json.schema.ValidationException;
import java.util.List;
import java.util.Arrays;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ValidationTest {
    EadValidationService validationService;

    private List<String> inValidExampleEads = Arrays.asList(
        "/sample-apps/sample_apps/invalid/solution_store/_001_invalid_type/ead.json", 
        "/sample-apps/sample_apps/invalid/solution_store/_002_invalid_reference/ead.json", 
        "/sample-apps/sample_apps/invalid/solution_store/_003_invalid_reference/ead.json", 
        "/sample-apps/sample_apps/invalid/solution_store/_004_invalid_incomplete_output/ead.json", 
        "/sample-apps/sample_apps/invalid/solution_store/_005_invalid_classes_value/ead.json",
        "/sample-apps/sample_apps/invalid/solution_store/_006_invalid_namespace/ead.json"
    );

    private List<String> validExampleEads = Arrays.asList(
        "/sample-apps/sample_apps/valid/solution_store/_001_valid/ead.json", 
        "/sample-apps/sample_apps/valid/solution_store/_002_valid/ead.json", 
        "/sample-apps/sample_apps/valid/solution_store/_003_valid/ead.json", 
        "/sample-apps/sample_apps/valid/solution_store/_004_valid/ead.json", 
        "/sample-apps/sample_apps/valid/solution_store/_005_valid/ead.json",
        "/sample-apps/sample_apps/valid/tutorial/_01_simple_app/ead.json",
        "/sample-apps/sample_apps/valid/tutorial/_02_collections_and_references/ead.json",
        "/sample-apps/sample_apps/valid/tutorial/_03_annotation_results/ead.json",
        "/sample-apps/sample_apps/valid/tutorial/_04_output_references_output/ead.json",
        "/sample-apps/sample_apps/valid/tutorial/_05_classes/ead.json",
        "/sample-apps/sample_apps/valid/tutorial/_06_large_collections/ead.json",
        "/sample-apps/sample_apps/valid/tutorial/_07_configuration/ead.json"
        
    );

    private String getValidExampleJson( int example_idx ) {
        InputStream exampleJson = getClass().getResourceAsStream(
            this.validExampleEads.get(example_idx)
        );
        return new JSONObject( new JSONTokener( exampleJson ) ).toString( 4 );
    }

    private String getInvalidExampleJson( int example_idx ) {
        InputStream exampleJson = getClass().getResourceAsStream(
            this.inValidExampleEads.get(example_idx)
        );
        return new JSONObject( new JSONTokener( exampleJson ) ).toString( 4 );
    }

    @BeforeEach
    void setUp() {
        ValidationProperties properties = new ValidationProperties();
        properties.setValidateNamespaceReferences( false );
        properties.setNamespaceDataSourceService( null );

        ChecksumService checksumService = new ChecksumServiceImpl();

        try {
            validationService = new EadValidationServiceImpl( properties, checksumService );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName( "Bad parameters test" )
    void badParametersTest() {
        ValidationProperties properties = new ValidationProperties();
        properties.setValidateNamespaceReferences( true );
        properties.setNamespaceDataSourceService( null );

        assertThrows( NullPointerException.class, () -> new EadValidationServiceImpl( properties, null ) );
    }

    @Test
    @DisplayName( "Basic EAD - Valid" )
    void validExample1ValidationTest() {
        String example = getValidExampleJson( 0 );
        validationService.validateAndParse( example );
    }

    @Test
    @DisplayName( "Collections EAD - Valid" )
    void validExample2ValidationTest() {
        String example = getValidExampleJson( 1 );
        validationService.validateAndParse( example );
    }

    @Test
    @DisplayName( "References EAD - Valid" )
    void validExample3ValidationTest() {
        String example = getValidExampleJson( 2 );
        validationService.validateAndParse( example );
    }

    @Test
    @DisplayName( "Classes EAD - Valid" )
    void validExample4ValidationTest() {
        String example = getValidExampleJson( 3 );
        validationService.validateAndParse( example );
    }

    @Test
    @DisplayName( "Large Collections EAD - Valid" )
    void validExample5ValidationTest() {
        String example = getValidExampleJson( 4 );
        validationService.validateAndParse( example );
    }

    @DisplayName( "Tutorial app 01 - Valid" )
    void validTutorialApp01ValidationTest() {
        String example = getValidExampleJson( 5 );
        validationService.validateAndParse( example );
    }

    @Test
    @DisplayName( "Tutorial app 02 - Valid" )
    void validTutorialApp02ValidationTest() {
        String example = getValidExampleJson( 6 );
        validationService.validateAndParse( example );
    }

    @Test
    @DisplayName( "Tutorial app 03 - Valid" )
    void validTutorialApp03ValidationTest() {
        String example = getValidExampleJson( 7 );
        validationService.validateAndParse( example );
    }

    @Test
    @DisplayName( "Tutorial app 04 - Valid" )
    void validTutorialApp04ValidationTest() {
        String example = getValidExampleJson( 8 );
        validationService.validateAndParse( example );
    }

    @Test
    @DisplayName( "Tutorial app 05 - Valid" )
    void validTutorialApp05ValidationTest() {
        String example = getValidExampleJson( 9 );
        validationService.validateAndParse( example );
    }

    @Test
    @DisplayName( "Tutorial app 06 - Valid" )
    void validTutorialApp06ValidationTest() {
        String example = getValidExampleJson( 10 );
        validationService.validateAndParse( example );
    }

    @Test
    @DisplayName( "Tutorial app 07 - Valid" )
    void validTutorialApp07ValidationTest() {
        String example = getValidExampleJson( 11 );
        validationService.validateAndParse( example );
    }

    @Test
    @DisplayName( "Basic EAD - Not Valid" )
    void invalidExample1ValidationTest() {
        String example = getInvalidExampleJson( 0 );
        assertThrows( ValidationException.class, () -> validationService.validateAndParse( example ) );
    }

    @Test
    @DisplayName( "Collections EAD - Not Valid" )
    void invalidExample2ValidationTest() {
        String example = getInvalidExampleJson( 1 );
        assertThrows( EADValidationException.class, () -> validationService.validateAndParse( example ) );
    }

    @Test
    @DisplayName( "References EAD - Not Valid" )
    void invalidExample3ValidationTest() {
        String example = getInvalidExampleJson( 2 );
        assertThrows( EADValidationException.class, () -> validationService.validateAndParse( example ) );
    }

    @Test
    @DisplayName( "Classes EAD - Not Valid" )
    void invalidExample4ValidationTest() {
        String example = getInvalidExampleJson( 3 );
        assertThrows( ValidationException.class, () -> validationService.validateAndParse( example ) );
    }

    @Test
    @DisplayName( "Large Collections EAD - Not Valid" )
    void invalidExample5ValidationTest() {
        String example = getInvalidExampleJson( 4 );
        assertThrows( ValidationException.class, () -> validationService.validateAndParse( example ) );
    }

    @Test
    @DisplayName( "Namespace - Not Valid" )
    void invalidExample6ValidationTest() {
        String example = getInvalidExampleJson( 5 );
        assertThrows( ValidationException.class, () -> validationService.validateAndParse( example ) );
    }
}
