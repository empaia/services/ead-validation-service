#!/usr/bin/env python3

import pathlib
import xml.etree.ElementTree as ET


pom_file = pathlib.Path("pom.xml")
tree = ET.parse(pom_file)
root = tree.getroot()
version = root.findall("{http://maven.apache.org/POM/4.0.0}version")[0].text
print(version)
