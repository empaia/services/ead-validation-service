FROM openjdk:14-alpine
COPY . /root/ead-val-service
WORKDIR  /root/ead-val-service
ARG MAVEN_OPTS
RUN export MAVEN_OPTS && ./mvnw install
RUN mv target/ead-validation.jar ead-validation.jar
COPY src/main/resources/schemas /root/ead-val-service/schemas
CMD "java -jar ead-validation.jar 