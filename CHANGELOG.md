# 0.1.3

- update ci

# 0.1.2

- added MAVEN_OPTS build arg to Dockerfile

# 0.1.1

- schema: require reference for annotations and classes

# 0.1.0

- added sample-apps submodule (and therefore more tests)
- POST `/api/v0/validate`
- GET `/alive`
- schema v3: removed `default` and `optional` keywords for input/output data types
