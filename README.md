# Ead Validation

## Setup

```bash
git clone --recurse-submodules https://gitlab.cc-asp.fraunhofer.de/empaia/platform/common-services/workbench/ead-validation-service.git
```

## How to use

There is a single endpoint `/api/v0/validate` where you can send a post request with the Empaia App Description as body.

This endpoint replies with `200` and the checksum of the classes if the ead was valid, if the ead is not valid it will send a `400` with an error message explaining why it failed.

## Build

```bash
./mvnw install
```

Example:

```sh
curl --request POST "<host>:8080/api/v0/validate" --header "Content-Type: application/json" --data-raw "{ <the ead> }"
```

## Configuration

By default, the server is using the baked in json schema to validate, however you can provide your own schema 
(only loaded at runtime) to validate the json.

To specify your own schema you can overwrite `eadValidation.schemaLocation` parameter to the file you want to use, 
it will work with Unix and Windows paths

Example:

`java -DeadValidation.schemaLocation=/path/to/schema.json -jar ead-validation-0.0.1-SNAPSHOT.jar`

> Note: Providing your own schema won't modify the reference and mapping checks

## Docker

Place new ead-schemas at `src/main/resources/schemas` if needed.

Edit port, schema to use in `docker-compose.yml`

```bash
docker-compose up --build
```
